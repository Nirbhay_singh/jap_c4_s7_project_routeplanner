package src.jap;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Scanner;

public class FlightMainClass {
    void displayDetails(){
        String value;
        try{
            BufferedReader br=new BufferedReader(new FileReader("routes.csv"));
            while ((value=br.readLine())!=null){
                String[] routes =value.split(",");

                String from=routes[0];
                String to=routes[1];
                String distance=routes[2];
                String travel_time=routes[3];
                String airfare=routes[4];

                System.out.println(from+" "+to+" "+distance+" "+travel_time+" "+airfare);
            }
            //System.out.println();
        }catch (Exception e){
            System.out.println(e);
        }
    }

    void showDirectFlights(String fromCity){
        String value1;
        try{
            BufferedReader br=new BufferedReader(new FileReader("routes2.csv"));
            FileWriter fileWriter=new FileWriter("routes1.txt");
            while ((value1=br.readLine())!=null){
                String[] routes =value1.split(",");

                String from=routes[0];
                String to=routes[1];
                String distance=routes[2];
                String travel_time=routes[3];
                String airfare=routes[4];

                        if (fromCity.equals(from)) {
                            System.out.println(from + " " + to + " " + distance + " " + travel_time + " " + airfare);
                            fileWriter.write(from+" ");
                            fileWriter.write(to+" ");
                            fileWriter.write(distance+" ");
                            fileWriter.write(travel_time+" ");
                            fileWriter.write(airfare+" ");
                            fileWriter.write("\n");
                        }



            }
            fileWriter.close();
        }catch (Exception e){
            System.out.println(e);
        }
    }
    void sortDirectFlights(){
        String value2;
        try{
            BufferedReader br=new BufferedReader(new FileReader("routes1.txt"));
            while ((value2=br.readLine())!=null){
                System.out.println(value2);
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }
    void analyzeAvailableRoute(String sourceCity,String destinationCity){
        String value3;
        try{
            BufferedReader br=new BufferedReader(new FileReader("routes2.csv"));
            while ((value3=br.readLine())!=null){
                String[] routes =value3.split(",");

                String from=routes[0];
                String to=routes[1];
                String distance=routes[2];
                String travel_time=routes[3];
                String airfare=routes[4];
                if(sourceCity.equals(from)&&destinationCity.equals(to))
                    System.out.println(from+" "+to+" "+distance+" "+travel_time+" "+airfare);
                else if(sourceCity.equals(from))
                    System.out.println(from+" "+to+" "+distance+" "+travel_time+" "+airfare);
                else if(destinationCity.equals(to))
                    System.out.println(from+" "+to+" "+distance+" "+travel_time+" "+airfare);
                else{
                    System.out.println("We are sorry. At this point of time, we do not have any information on flights from "+sourceCity+" to "+destinationCity+".");
                    break;
                }
            }
        }catch (Exception e){
            System.out.println(e);
        }
    }

    public static void main(String[] args) {
        FlightMainClass flightMainClass=new FlightMainClass();
        Scanner scanner=new Scanner(System.in);
        System.out.println("\n===========================Flight Details============================= \n");
        flightMainClass.displayDetails();
        System.out.println("\n===========================Direct Flight==============================");
        System.out.println("\nEnter the Source City:");
        String sourcecity=scanner.nextLine();
        System.out.println("From To Distance Travel Time Airfare");
        flightMainClass.showDirectFlights(sourcecity);
        System.out.println("\n===========================Sorted Direct Flight========================");
        flightMainClass.sortDirectFlights();
        System.out.println("\n====================Available Routes from Source to destination==========");
        System.out.println("\nEnter the Source City:");
        String SourceCity=scanner.nextLine();
        System.out.println("Enter the Destination City:");
        String DestinationCity=scanner.nextLine();
        System.out.println("From To Distance Travel Time Airfare");
        flightMainClass.analyzeAvailableRoute(SourceCity,DestinationCity);

    }
}
